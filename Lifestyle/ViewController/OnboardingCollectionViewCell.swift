//
//  OnboardingCollectionViewCell.swift
//  Lifestyle
//
//  Created by Матвей Малацион on 06/10/2018.
//  Copyright © 2018 Lifestyle. All rights reserved.
//

import UIKit

class OnboardingCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var innerConstraint: NSLayoutConstraint!
}
