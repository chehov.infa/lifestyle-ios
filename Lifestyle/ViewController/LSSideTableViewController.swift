//
//  LSSideTableViewController.swift
//  Lifestyle
//
//  Created by Матвей Малацион on 03.09.2018.
//  Copyright © 2018 Lifestyle. All rights reserved.
//

import UIKit

@objc
protocol LSSideMenuDelegate : NSObjectProtocol{
    func goOrders()
    func goReviews()
    func goInvite()
    func goSettings()
    func goCity()
    func goBonus()
    func goCalc()
    func goCall()
    func goHelp()
    func goLogin()
    func goLogout()
}

class LSSideTableViewController: FLFlexibleTableViewController {
    
    var api : LSApi!
    
    
    var sections : [[String : Any]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.separatorColor = UIColor.clear
        
        self.setFake()
        
    }
    
    var badge = 0
    
    var user : User?
    
    func setFake(){
        if let userIdCookie = HTTPCookieStorage.shared.cookies?.first(where: { c in c.name == "userId"}) {
            self.structure = self.authedStracture(user: user)
        }else{
            self.structure = self.loadingItems()
            
        }
        self.tableView.reloadData()
    }
    
    @objc override func refresh(){
        
       setFake()
        
        let success : (User)->() = {user in
            self.user = user
            self.buildAuthedUI(user: user)
             DispatchQueue.main.async {
                self.refreshControl?.endRefreshing()
            }
        }
        
        let failure : (Error?, ErrorReason)->() = { err, reason in
            self.buildUnauthedUI()
             DispatchQueue.main.async {
                self.refreshControl?.endRefreshing()
            }
        }
        
        let badge : (Int)->() = {badge in
            
            self.badge = badge
            if(self.user != nil){
                self.buildAuthedUI(user: self.user!)
            }
            DispatchQueue.main.async {
                self.refreshControl?.endRefreshing()
            }
        }
        
        api.getUserInfo(success: success, failure: failure)
        api.getBadgeValue(success: badge, failure: failure)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        //        self.view.bounds = CGRect(x: 0, y: 0, width: self.view.window?.bounds.width ?? 0, height: self.view.bounds.height)
    }
    
    var delegate : LSSideMenuDelegate? {
        didSet{
            self.performer = delegate as! NSObject
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.refresh()
        
    }
    
    
    
    func buildAuthedUI(user : User){
        DispatchQueue.main.async {
            self.structure = self.authedStracture(user: user)
            
            self.tableView.reloadData()
        }
        
    }
    
    func buildUnauthedUI(){
        DispatchQueue.main.async {
            self.structure = self.unauthedStructure()
            self.tableView.reloadData()
        }
    }
    
    func unauthedStructure() -> [String : Any] {
        return ["Sections": [
            [   "Name" : "" , "Items" : [
                [
                    "Title" : "Войти",
                    "HasDisclosure" : false,
                    "Tint" : UIColor.white,
                    "BackgroundColor" : UIColor.accent,
                    "Height" : CGFloat(68),
                    "TextColor" : UIColor.white,
                    "Image" : #imageLiteral(resourceName: "avatar"),
                    "Action" : #selector(self.delegate?.goLogin)
                ]
                ]
                
            ],[ "Name" : "",
                "Items" : [
                    [
                        "Title" : "Выбор города    Казань",
                        "Subtitle" : "",
                        "CellStyle" : UITableViewCellStyle.value1.rawValue,
                        "HasDisclosure" : false,
                        "Tint" : UIColor.white,
                        "BackgroundColor" : UIColor.white,
                        "Height" : CGFloat(48),
                        "TextColor" : UIColor.black.withAlphaComponent(0.6),
                        "Action" : #selector(self.delegate?.goCity)
                        //                        "Action" : #selector(self.selectCashback)
                    ],
                    [
                        "Title" : "Бонусная программа",
                        "Subtitle" : "",
                        "HasDisclosure" : false,
                        "Tint" : UIColor.white,
                        "BackgroundColor" : UIColor.white,
                        "Height" : CGFloat(48),
                        "TextColor" : UIColor.black.withAlphaComponent(0.6),
                        "Action" : #selector(self.delegate?.goBonus)
                        //                        "Action" : #selector(self.selectCashback)
                    ],
                    [
                        "Title" : "Калькулятор калорий",
                        "Subtitle" : "",
                        "HasDisclosure" : false,
                        "Tint" : UIColor.white,
                        "BackgroundColor" : UIColor.white,
                        "Height" : CGFloat(48),
                        "TextColor" : UIColor.black.withAlphaComponent(0.6),
                        "Action" : #selector(self.delegate?.goCalc)
                        //                        "Action" : #selector(self.selectCashback)
                    ],
                    [
                        "Title" : "Позвонить",
                        "Subtitle" : "",
                        "HasDisclosure" : false,
                        "Tint" : UIColor.white,
                        "BackgroundColor" : UIColor.white,
                        "Height" : CGFloat(48),
                        "TextColor" : UIColor.black.withAlphaComponent(0.6),
                        "Action" : #selector(self.delegate?.goCall)
                        //                        "Action" : #selector(self.selectCashback)
                    ],
                    [
                        "Title" : "Помощь",
                        "Subtitle" : "",
                        "HasDisclosure" : false,
                        "Tint" : UIColor.white,
                        "BackgroundColor" : UIColor.white,
                        "Height" : CGFloat(48),
                        "TextColor" : UIColor.black.withAlphaComponent(0.6),
                        "Action" : #selector(self.delegate?.goHelp)
                        //                        "Action" : #selector(self.selectCashback)
                    ],
                ]
            ]
            ] as Any
        ]
    }
    
    
    func authedStracture(user : User?) -> [String : Any]{
        
        let i1 = [
            "Title" : user?.name ?? "",
            "Subtitle" : (user != nil) ? "\(user!.points ?? 0) баллов" : "",
            "HasDisclosure" : false,
            "Tint" : UIColor.white,
            "BackgroundColor" : UIColor.accent,
            "Height" : CGFloat(68),
            "TextColor" : UIColor.white,
            "Image" : #imageLiteral(resourceName: "avatar"),
            "WebImage" : user?.image,
            "Action" : #selector(self.delegate?.goOrders)
            ] as [String : Any]
        
        let i2 = [
            "Title" : "Заказы",
            "Subtitle" : "",
            "HasDisclosure" : false,
            "BackgroundColor" : UIColor.white,
            "Height" : CGFloat(48),
            "TextColor" : UIColor.black,
            "Action" : #selector(self.delegate?.goOrders),
            "Image" : #imageLiteral(resourceName: "list")
            ] as [String : Any]
        
        let i3 = [
            "Title" : "Мои отзывы",
            "Subtitle" : "",
            "HasDisclosure" : false,
            "Tint" : UIColor.white,
            "BackgroundColor" : UIColor.white,
            "Height" : CGFloat(48),
            "TextColor" : UIColor.black,
            "Action" : #selector(self.delegate?.goReviews),
            "BadgeValue" : self.badge,
            "Image" : #imageLiteral(resourceName: "review")
            //                        "Action" : #selector(self.selectCashback)
            ] as [String : Any]
        
        let i4 = [
            "Title" : "Пригласить друга",
            "Subtitle" : "",
            "HasDisclosure" : false,
            "Tint" : UIColor.white,
            "BackgroundColor" : UIColor.white,
            "Height" : CGFloat(48),
            "TextColor" : UIColor.black,
            "Action" : #selector(self.delegate?.goInvite),
            "Image" :  UIImage(named: "profile")
            //                        "Action" : #selector(self.selectCashback)
            ] as [String : Any]
        
        let i5 =
            [
                "Title" : "Настройки",
                "Subtitle" : "",
                "HasDisclosure" : false,
                "Tint" : UIColor.white,
                "BackgroundColor" : UIColor.white,
                "Height" : CGFloat(48),
                "TextColor" : UIColor.black,
                "Action" : #selector(self.delegate?.goSettings),
                "Image" : #imageLiteral(resourceName: "gear")
                //                        "Action" : #selector(self.selectCashback)
                ] as [String : Any]
        
        let i6 = [
            "Title" : "Выйти",
            "Subtitle" : "",
            "HasDisclosure" : false,
            "Tint" : UIColor.white,
            "BackgroundColor" : UIColor.white,
            "Height" : CGFloat(48),
            "TextColor" : UIColor.black,
            "Action" : #selector(self.delegate?.goLogout),
            "Image" : #imageLiteral(resourceName: "exit")
            //                        "Action" : #selector(self.selectCashback)
            ] as [String : Any]
        
        let sectionItems1 = [
            i1,
            i2,
            i3,
            i4,
            i5,
            i6
        ]
        
        let sectionItems2 = [
            [
                "Title" : "Выбор города    Казань",
                "Subtitle" : "",
                "HasDisclosure" : false,
                "CellStyle" : UITableViewCellStyle.value1.rawValue,
                "Tint" : UIColor.white,
                "BackgroundColor" : UIColor.white,
                "Height" : CGFloat(48),
                "TextColor" : UIColor.black.withAlphaComponent(0.6),
                "Action" : #selector(self.delegate?.goCity)
                //                        "Action" : #selector(self.selectCashback)
            ],
            [
                "Title" : "Бонусная программа",
                "Subtitle" : "",
                "HasDisclosure" : false,
                "Tint" : UIColor.white,
                "BackgroundColor" : UIColor.white,
                "Height" : CGFloat(48),
                "TextColor" : UIColor.black.withAlphaComponent(0.6),
                "Action" : #selector(self.delegate?.goBonus)
                //                        "Action" : #selector(self.selectCashback)
            ],
            [
                "Title" : "Калькулятор калорий",
                "Subtitle" : "",
                "HasDisclosure" : false,
                "Tint" : UIColor.white,
                "BackgroundColor" : UIColor.white,
                "Height" : CGFloat(48),
                "TextColor" : UIColor.black.withAlphaComponent(0.6),
                "Action" : #selector(self.delegate?.goCalc)
                //                        "Action" : #selector(self.selectCashback)
            ],
            [
                "Title" : "Позвонить",
                "Subtitle" : "",
                "HasDisclosure" : false,
                "Tint" : UIColor.white,
                "BackgroundColor" : UIColor.white,
                "Height" : CGFloat(48),
                "TextColor" : UIColor.black.withAlphaComponent(0.6),
                "Action" : #selector(self.delegate?.goCall)
                //                        "Action" : #selector(self.selectCashback)
            ],
            [
                "Title" : "Помощь",
                "Subtitle" : "",
                "HasDisclosure" : false,
                "Tint" : UIColor.white,
                "BackgroundColor" : UIColor.white,
                "Height" : CGFloat(48),
                "TextColor" : UIColor.black.withAlphaComponent(0.6),
                "Action" : #selector(self.delegate?.goHelp)
                //                        "Action" : #selector(self.selectCashback)
            ]
        ] as Array<Any>
        
        let sections = [
            [   "Name" : "" ,
                "Items" : sectionItems1
            ],[ "Name" : "",
                "Items" : sectionItems2
            ]
        ]
        
        return ["Sections": sections]
        
        
    }
    
    
    func loadingItems() -> [String : Any]{
        return ["Sections": [
            [ "Name" : "",
                "Items" : [
                    [
                        "Title" : "Выбор города    Казань",
                        "Subtitle" : "",
                        "HasDisclosure" : false,
                        "CellStyle" : UITableViewCellStyle.value1.rawValue,
                        "Tint" : UIColor.white,
                        "BackgroundColor" : UIColor.white,
                        "Height" : CGFloat(48),
                        "TextColor" : UIColor.black.withAlphaComponent(0.6)
                        //                        "Action" : #selector(self.selectCashback)
                    ],
                    [
                        "Title" : "Бонусная программа",
                        "Subtitle" : "",
                        "HasDisclosure" : false,
                        "Tint" : UIColor.white,
                        "BackgroundColor" : UIColor.white,
                        "Height" : CGFloat(48),
                        "TextColor" : UIColor.black.withAlphaComponent(0.6)
                        //                        "Action" : #selector(self.selectCashback)
                    ],
                    [
                        "Title" : "Калькулятор калорий",
                        "Subtitle" : "",
                        "HasDisclosure" : false,
                        "Tint" : UIColor.white,
                        "BackgroundColor" : UIColor.white,
                        "Height" : CGFloat(48),
                        "TextColor" : UIColor.black.withAlphaComponent(0.6)
                       
                        //                        "Action" : #selector(self.selectCashback)
                    ],
                    [
                        "Title" : "Позвонить",
                        "Subtitle" : "",
                        "HasDisclosure" : false,
                        "Tint" : UIColor.white,
                        "BackgroundColor" : UIColor.white,
                        "Height" : CGFloat(48),
                        "TextColor" : UIColor.black.withAlphaComponent(0.6)
                        //                        "Action" : #selector(self.selectCashback)
                    ],
                    [
                        "Title" : "Помощь",
                        "Subtitle" : "",
                        "HasDisclosure" : false,
                        "Tint" : UIColor.white,
                        "BackgroundColor" : UIColor.white,
                        "Height" : CGFloat(48),
                        "TextColor" : UIColor.black.withAlphaComponent(0.6)
                        //                        "Action" : #selector(self.selectCashback)
                    ],
                ]
            ]
            ] as Any
        ]
    }
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

