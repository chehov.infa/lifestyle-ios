//
//  FLFlexibleTableViewController.swift
//  FlashkaCashbox
//
//  Created by Матвей Малацион on 28.10.17.
//  Copyright © 2017 mediaPro. All rights reserved.
//

import UIKit
import SDWebImage

class FLFlexibleTableViewController: UITableViewController {
    
    public var structure : [String : Any]?
    var defaultStyle : UITableViewCellStyle = UITableViewCellStyle.subtitle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UILabel.appearance(whenContainedInInstancesOf: [UITableViewHeaderFooterView.self]).textAlignment = NSTextAlignment.center
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action:#selector(refresh) , for: .valueChanged)
        
    }
    
    @objc func refresh(){
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        if let sections : [[String : Any]] = self.structure?["Sections"] as? [[String : Any]] {
            return sections.count
        } else {
            return 0;
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = self.structure?["Sections"] as? [[String : Any]] {
            let section = sections[section]
            let items = section["Items"] as? [[String : Any]]
            return items!.count
        } else {
            return 0;
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let sections = self.structure?["Sections"] as? [[String : Any]] {
            let section = sections[indexPath.section]
            let items = section["Items"] as? [[String : Any]]
            let item = items?[indexPath.row]
            
            if let custom = item?["Custom"] as? [String : Any] {
                
                let nib = custom["Nib"] as! String
                
                tableView.register(UINib (nibName: nib, bundle: nil), forCellReuseIdentifier: nib)
                let cell = tableView.dequeueReusableCell(withIdentifier: nib)!
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                
                if let filler = custom["Filler"] as? Selector {
                    self.perform(filler, with: cell)
                }
                
                return cell
                
            } else{
                
                let cell = { () -> UITableViewCell in
//                    if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: UITableViewCell.self))  {
//                        return cell
//                    } else {
                    if let cellStyle = item?["CellStyle"] as? Int{
                        let cell = UITableViewCell(style: UITableViewCellStyle(rawValue: cellStyle)!, reuseIdentifier: String(describing: UITableViewCell.self))
                        return cell
                    }else
                        if((item?["Subtitle"]) != nil){
                            let cell = UITableViewCell(style: defaultStyle, reuseIdentifier: String(describing: UITableViewCell.self))
                            return cell
                        }else{
                            let cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: String(describing: UITableViewCell.self))
                            return cell
                        }
//                    }
                } ()
                
                if let hasDisclosure = item?["HasDisclosure"] {
                    if hasDisclosure as! Bool {
                        cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                    } else {
                        cell.accessoryType =  UITableViewCellAccessoryType.none
                    }
                }
                
                
                
                cell.textLabel?.text = item?["Title"] as? String
                cell.detailTextLabel?.text = item?["Subtitle"] as? String
                cell.detailTextLabel?.textColor = item?["Tint"] as? UIColor
                cell.textLabel?.textColor = item?["TextColor"] as? UIColor ?? UIColor.black
                cell.backgroundColor = (item?["BackgroundColor"] as? UIColor) ?? UIColor.white
                cell.imageView?.image = item?["Image"] as? UIImage
                
                cell.viewWithTag(999)?.removeFromSuperview()
                
                if let badgeValue = item?["BadgeValue"] as? Int{
                    
                    if badgeValue == 0{
                        
                    }else{
                        let label = UILabel()
                        label.tag = 999
                        label.text = badgeValue.description
                        label.textColor = UIColor.white
                        label.backgroundColor = UIColor(red: 229/255.0, green: 57/255.0, blue: 53/255.0, alpha: 1)
                        label.bounds = CGRect(x: 0, y: 0, width: 24, height: 24)
                        label.layer.cornerRadius = 12
                        label.layer.masksToBounds = true
                        label.textAlignment = .center
                       
                        cell.addSubview(label)
                        
                        label.center = CGPoint(x:280, y: cell.center.y)

                    }
                    
                }else{
                    cell.imageView?.transform = CGAffineTransform.identity
                }
                
                if let image = item?["WebImage"] as? String{
                    //                    cell.imageView?.image = sdWeb
                    cell.imageView?.sd_setImage(with: URL(string: image), completed: { (img, err, type, url) in
                        if err != nil || self.tableView.indexPath(for: cell) != indexPath{
                            cell.imageView?.image = item?["Image"] as? UIImage
                        }
                    })
                }
                
                return cell
            }
        } else {
            return UITableViewCell();
        }
    }
    
    var performer : NSObject!
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView .deselectRow(at: indexPath, animated: true)
        
        if let sections = self.structure?["Sections"] as? [[String : Any]] {
            let section = sections[indexPath.section]
            let items = section["Items"] as? [[String : Any]]
            let item = items?[indexPath.row]
            
            if let action = item?["Action"] as? Selector {
                
                self.performer.perform(action, with: indexPath)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if let sections : [[String : Any]] = self.structure?["Sections"] as? [[String : Any]] {
            let section = sections[indexPath.section]
            let items = section["Items"] as? [[String : Any]]
            let item = items?[indexPath.row]
            
            if let custom = item?["Custom"] as? [String : Any]{
                return custom["Height"] as! CGFloat
            }
            
            if let height = item?["Height"]{
                return height as! CGFloat
            }
        }
        
        return 50 //UITableViewAutomaticDimension
    }
    
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        
        if let sections : [[String : Any]] = self.structure?["Sections"] as? [[String : Any]] {
            let section = sections[section]
            return section["Name"] as? String
        }
        return ""
    }
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


